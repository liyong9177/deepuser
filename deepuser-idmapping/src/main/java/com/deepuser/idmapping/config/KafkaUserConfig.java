package com.deepuser.idmapping.config;

import com.deepuser.idmapping.service.impl.KafkaConsumerService;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.EnableKafka;


/**
 * kafka消息队列配置
 * @author yong.li
 */
@EnableKafka
@Configuration
@ConditionalOnProperty(prefix = "spring.kafka",value = "bootstrap-servers")
@Import({KafkaConsumerService.class})
public class KafkaUserConfig {

    /**
     * 创建Topic并设置分区数为8以及副本数为2
     *
     * @param topicName
     * @return
     */
    @Bean
    public NewTopic initialTopic(@Value("${spring.kafka.producer.gourd-topic1:deepuser}")String topicName) {
        return new NewTopic(topicName,8, (short) 2 );
    }

}