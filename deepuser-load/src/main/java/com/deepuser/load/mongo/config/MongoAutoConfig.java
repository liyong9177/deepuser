package com.deepuser.load.mongo.config;

import com.deepuser.load.mongo.service.impl.UserMogServiceImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({UserMogServiceImpl.class})
public class MongoAutoConfig {

//    @Bean
//    public UserMogService userMogService(){
//        return  new UserMogServiceImpl();
//    }
    /**
     * 副本集模式支持
     * @param factory
     * @return
     */
    /*@Bean
    MongoTransactionManager transactionManager(MongoDbFactory factory){
        return new MongoTransactionManager(factory);
    }*/

}