package com.deepuser.load.mongo.service;


import com.deepuser.load.mongo.model.UserMog;

import java.util.HashMap;

/**
 * @Description mongo用户服务类
 * @Date 2020/7/22 17:24
 * @Version 1.0
 */
public interface UserMogService {

    void update(UserMog e) throws Exception;
}
