package com.deepuser.load.mongo.service.impl;

import com.deepuser.load.mongo.model.UserMog;
import com.deepuser.load.mongo.model.UserTag;
import com.deepuser.load.mongo.repository.UserRepository;
import com.deepuser.load.mongo.service.UserMogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @Description mongo用户服务类
 * @Author gourd.hu
 * @Date 2020/7/22 17:24
 * @Version 1.0
 */

@Service("userMogService")
@Slf4j
public class UserMogServiceImpl implements UserMogService {

    @Autowired
    private UserRepository userRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(UserMog userMog ) throws Exception {
        List<UserMog> userMogs = userRepository.findBySuperId(userMog.getSuperId().toString());
        if (null==userMogs || userMogs.size()==0){
            userRepository.save(userMog);
            return;
        }
        UserMog result = new UserMog();
        result.setId(userMogs.get(0).getId());
        result.setSuperId(userMogs.get(0).getSuperId().toString());
        result.setPhone(StringUtils.isEmpty(null ==userMog.getPhone()?null:userMog.getPhone())?userMogs.get(0).getPhone():userMog.getPhone());
        //危险vvvvvv
        List<String> idList = (List<String>) userMogs.get(0).getIdList();
        List<String> resultList = new ArrayList<>();
        if (null != idList && idList.size()!=0){
            userMog.getIdList().forEach(item ->{
                if(!idList.contains(item)){
                    resultList.add(item);
                }
            });
            idList.addAll(resultList);
            result.setIdList(idList.stream().distinct().collect(Collectors.toList()));
        }
        //危险^^^^^^^
        HashMap<String, UserTag> userTagHashMap = new HashMap<>();
        ((HashMap<String,UserTag>) userMogs.get(0).getHashMap()).keySet().forEach(item ->{
            if(userMog.getHashMap().containsKey(item)){
                //更具创建时间更新
                if (userMog.getHashMap().get(item).getCreateTime() != null &&
                        userMog.getHashMap().get(item).getCreateTime() > ((HashMap<String,UserTag>)userMogs.get(0).getHashMap()).get(item).getCreateTime()){
                    userTagHashMap.put(item,userMog.getHashMap().get(item));
                }else{
                    userTagHashMap.put(item,((HashMap<String,UserTag>)userMogs.get(0).getHashMap()).get(item));
                }
            }else {
                userTagHashMap.put(item,((HashMap<String,UserTag>)userMogs.get(0).getHashMap()).get(item));
            }

        });
        userMog.getHashMap().keySet().forEach(item ->{
            if (!userTagHashMap.containsKey(item)){
                userTagHashMap.put(item,((HashMap<String,UserTag>)userMogs.get(0).getHashMap()).get(item));
            }
        });
        result.setHashMap(userTagHashMap);
        log.info("插入数据{}",result.toString());
        userRepository.save(result);
    }
}
