package com.deepuser.load.mongo.model;

import lombok.Data;

@Data
public class UserTag {
    /**
     * 标签名
     */
    private String name;
    /**
     * 标签英文名
     */
    private String englishName;

    /**
     * 标签值
     */
    private String value;
    /**
     * 失效事件
     */
    private Long invalidTime;
    /**
     * 创建时间
     */
    private Long createTime;

}
