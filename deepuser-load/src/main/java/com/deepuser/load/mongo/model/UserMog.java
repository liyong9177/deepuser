package com.deepuser.load.mongo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * mongo用户对象
 *
 * @author gourd.hu
 * @date 2019-04-02 17:26:16
 */
@Data
@Document("user")
public class UserMog {

    @Id
    private String id;

    /**
     * 手机
     */
    private String phone;


    /**
     * IDList
     */
    private List<String> idList;

    /**
     * superId
     */
    private String superId;


    private HashMap<String,UserTag> hashMap;
}