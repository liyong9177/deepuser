package com.deepuser.load.mongo.repository;

import com.deepuser.load.mongo.model.UserMog;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @Description 用户接口
 * @Author gourd.hu
 * @Date 2020/7/22 16:04
 * @Version 1.0
 */
public interface UserRepository extends MongoRepository<UserMog,Long> {


    /**
     * 根据superid查询数据
     * @param ages
     * @return
     */
    List<UserMog> findBySuperId(String ages);


}
