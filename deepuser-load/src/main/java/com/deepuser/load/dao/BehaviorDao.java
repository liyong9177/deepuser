package com.deepuser.load.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.deepuser.load.model.Behavior;
import org.springframework.stereotype.Repository;

@Repository
public interface BehaviorDao extends BaseMapper<Behavior> {
    int insertData(Behavior entity);
}
