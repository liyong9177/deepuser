package com.deepuser.load.model;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="user_behavior",excludeProperty="id")
public class Behavior {
    private Long id;
    //客户端
    private String appkey;
    //事件
    private String actionname;
    //json内容
    private Object data;
    //时间戳
    private Long timestemp;
    //superId
    private String superId;
}
