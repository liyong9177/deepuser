package com.deepuser.load.config;

import com.deepuser.load.service.KafkaBehaviorService;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@ConditionalOnProperty(prefix = "spring.kafka",value = "bootstrap-servers")
@Import({ KafkaBehaviorService.class})
public class KafkaBehaviorConfig {

    /**
     * 创建Topic并设置分区数为8以及副本数为2
     *
     * @param topicName
     * @return
     */
    @Bean
    public NewTopic initialTopic(@Value("${spring.kafka.producer.gourd-topic1:deepuser}")String topicName) {
        return new NewTopic(topicName,8, (short) 2 );
    }

}