package com.deepuser.annotation;

import java.lang.annotation.*;


/**
 * @功能描述 防止重复提交标记注解
 * @author gourd.hu
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NoRepeatSubmit {
    /**
     * 重复统计时长
     * @return
     */
    int value() default 1;
}