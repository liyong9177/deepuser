package com.deepuser.base.dto;

import com.deepuser.enums.SortEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 查询分页基础类
 * @author gourd.hu
 */
@Data
@ApiModel(value = "查询分页基础对象", description = "查询分页基础对象")
public class BaseFindDTO {

    @ApiModelProperty("页数，默认值 1")
    private Integer pageNo =1;

    @ApiModelProperty("每页条数，默认值 10")
    private Integer pageSize =10;

    @ApiModelProperty("排序字段")
    private String orderColumn;

    @ApiModelProperty("排序类型")
    private SortEnum orderType;

}
