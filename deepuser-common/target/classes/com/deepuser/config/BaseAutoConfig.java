package com.deepuser.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MyBatisPlusConfig.class, Swagger2Config.class, WebMvcConfig.class,FilterConfig.class})
public class BaseAutoConfig {

    /**
     * 不依赖cloud模块需要注册此bean
     * @return
     */
//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }



}