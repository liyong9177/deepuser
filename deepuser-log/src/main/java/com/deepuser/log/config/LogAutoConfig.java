package com.deepuser.log.config;

import com.deepuser.log.aop.LogAllAop;
import com.deepuser.log.aop.LogPointAop;
import com.deepuser.log.job.DelExpireLog;
import com.deepuser.log.service.impl.OperateLogServiceImpl;
import lombok.Data;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * 日志配置
 *
 * @author gourd.hu
 */

@Data
@Configuration
@EnableScheduling
@MapperScan({"com.deepuser.log.dao"})
@Import({LogAllAop.class, LogPointAop.class, DelExpireLog.class, OperateLogServiceImpl.class})
public class LogAutoConfig  {

}