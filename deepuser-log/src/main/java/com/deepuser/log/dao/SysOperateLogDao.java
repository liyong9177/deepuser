package com.deepuser.log.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.deepuser.log.entity.SysOperateLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @date 2018-11-24
 */
public interface SysOperateLogDao extends BaseMapper<SysOperateLog> {

    void deleteByIds(@Param("ids") List<Long> ids);

}
